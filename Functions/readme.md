# Functions

- Immutable: int, float, bool, str, tuple, and unicode
- Mutable: list, set, dict

[Functions](https://www.programiz.com/python-programming/function)
