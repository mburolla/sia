# Wednesday
Python data access.

# Function Key Swap *
To make your function keys work like "real" function keys, enter the computer's BIOS (press F2 on restart) and Disable the HotKey option.  This forces you to press the Fn key to change your speaker volume, however executing F5 works with one key press (versus two).

# Boilerplate
The term boilerplate refers to standardized text, copy, documents, methods, or procedures that may be used over again without making major changes to the original. A boilerplate is commonly used for efficiency and to increase standardization in the structure and language.

We can copy and paste the Python data access code, treat it like boilerplate code, resuse it for our purposes.

# Insert Notes
- No reason for us to keep track caluculate the next database ID, we can let the database do it for us: AI AutoIncrement column in MySqlWorkbench
- No reason to include the DB ID in our insert statement
```
def insertDemo(firstName, lastName):
  retval = 0
  conn = createConnection()
  with conn.cursor() as cursor: # "with" means we are using Python's context manager, so we don't have to close things.
    cursor.execute("insert into demo (firstname, lastname) values (%s, %s)", (firstName, lastName))
    retval = cursor.lastrowid # Get the ID of the row we just inserted.
    conn.commit() # Force the DB accept our change.
  return retval # Return the ID we just inserted.
```

# Select Notes
- Online JSON viewer: http://jsonviewer.stack.hu/

# Database Connections
- Make sure your app does not gobble up all the db connections!
- Amazon Aurora DB pricing is based on DB connections
- Once we reach the connection limit, nobody else will be able to access the database!  Don't be that person :-)
- https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraMySQL.Managing.Performance.html#AuroraMySQL.Managing.MaxConnections
- `with` keyword uses the Python Context Manager to close DB connections
- We don't want to "leak" database connections

# Lambda Filter Map *
See `lambda-filter-map-rg` for more information.
```
  l1 = list(filter(lambda p : p['name'] == 'Billy', peopleList))
  l2 = list(map(lambda x : x['name'], peopleList))
```

# Workflow: Writing Data Access Code in Python
- Write SQL using MySQLWorkbench (Make sure this is working and all good)
- Copy SQL into Python and add %s
- Execute Python code
- Check tables using MySQLWorkbench (Are we good?  Yes, move on...)

# Types of Backend Work
- Python: Web APIs
- SQL: Database
- CloudFormation: CI/CD Build/deploy
- Python: CRON jobs/scheduled tasks/background services/scheduled jobs 
  - CRON: Command Run On
  - [Cron Expressions](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)

