# Tuesday
Scope: Database and data access via Python

# DB Nulls *
Avoid nulls in the DB at all cost:
- Misleading context with joins
- Overhead with creating a bitmask for every row in MS SQL Server
- Nulls always creep up into the application layer at runtime

# Misc
- Enable: VSCode Autosave: Search "autosave" (Optionally set auto save delay)
