# 
# File: intro-ex-9.py
# Auth: Martin Burolla
# Date: 8/16/2021
# Desc: Uppers or Lowers
#

def main():
  inString = input('Enter String: ')
  if (inString.isupper()):
    print("True")
  else:
    print("False")

if __name__ == "__main__":  
  main()
