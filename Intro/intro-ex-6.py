# 
# File: intro-ex-6.py
# Auth: Martin Burolla
# Date: 8/16/2021
# Desc: Reverse Me
#

def main():
  myList = [11, 100, 101, 999, 1001]
  myList.reverse()
  print(myList)

if __name__ == "__main__":  
  main()
